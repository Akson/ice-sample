package com.carpy.ice.entity;

import com.alibaba.fastjson.annotation.JSONField;

public class OutputMediaConfig {

    @JSONField(name = "MediaURL")
    private String mediaURL;

    @JSONField(name = "Bitrate")
    private int bitrate;

    @JSONField(name = "Width")
    private int width;

    @JSONField(name = "Height")
    private int height;

    public OutputMediaConfig() {}

    public String getMediaURL() {
        return mediaURL;
    }

    public void setMediaURL(String mediaURL) {
        this.mediaURL = mediaURL;
    }

    public int getBitrate() {
        return bitrate;
    }

    public void setBitrate(int bitrate) {
        this.bitrate = bitrate;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
