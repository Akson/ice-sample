package com.carpy.ice.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class BasicConfig {

    @JSONField(name = "ACCESS KEY ID")
    private String accessKeyId;

    @JSONField(name = "ACCESS KEY SECRET")
    private String accessKeySecret;

    @JSONField(name = "REGION ID")
    private String regionID;

    @JSONField(name = "BUCKET NAME")
    private String bucketName;

    @JSONField(name = "VIDEO PATH")
    private String videoPath;

    @JSONField(name = "TEXT PATH")
    private  String textPath;

    @JSONField(name = "SUBTITLE PATH")
    private String subtitlePath;

    private String uniquePre;

    public BasicConfig() {
        this.uniquePre = new SimpleDateFormat("yyyyMMddHHmm_").format(new Date());
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getRegionID() {
        return regionID;
    }

    public void setRegionID(String regionID) {
        this.regionID = regionID;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getIceEndpoint() {
        return "ice." + this.regionID + ".aliyuncs.com";
    }

    public String getOssEndpoint() {
        return "oss-" + this.regionID + ".aliyuncs.com";
    }

    public String getUniquePre() {
        return uniquePre;
    }

    public void setUniquePre(String uniquePre) {
        this.uniquePre = uniquePre;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getTextPath() {
        return textPath;
    }

    public void setTextPath(String textPath) {
        this.textPath = textPath;
    }

    public String getSubtitlePath() {
        return subtitlePath;
    }

    public void setSubtitlePath(String subtitlePath) {
        this.subtitlePath = subtitlePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicConfig userInfo = (BasicConfig) o;
        return Objects.equals(accessKeyId, userInfo.accessKeyId) && Objects.equals(accessKeySecret, userInfo.accessKeySecret) && Objects.equals(regionID, userInfo.regionID) && Objects.equals(bucketName, userInfo.bucketName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessKeyId, accessKeySecret, regionID, bucketName);
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "\n\taccessKeyId = '" + accessKeyId + '\'' +
                "\n\taccessKeySecret = '" + accessKeySecret + '\'' +
                "\n\tregionID = '" + regionID + '\'' +
                "\n\ticeEndpoint = '" + this.getIceEndpoint() + '\'' +
                "\n\tbucketName = '" + bucketName + '\'' +
                "\n\tossEndpoint = '" + this.getOssEndpoint() + '\'' +
                "\n}";
    }
}
