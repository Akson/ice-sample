package com.carpy.ice.entity.timeline.effect;

import com.alibaba.fastjson.annotation.JSONField;

import javax.swing.text.MaskFormatter;

public class Transition extends Effect {

    @JSONField(serialize = false, deserialize = false)
    private static String subTypes[] = {
            "directional",
            "displacement",
            "windowslice",
            "bowTieVertical",
            "bowTieHorizontal",
            "simplezoom",
            "linearblur",
            "waterdrop",
            "glitchmemories",
            "polka",
            "perlin",
            "directionalwarp",
            "bounce_up",
            "bounce_down",
            "wiperight",
            "wipeleft",
            "wipedown",
            "wipeup",
            "morph",
            "colordistance",
            "circlecrop",
            "swirl",
            "dreamy",
            "gridflip",
            "zoomincircles",
            "radial",
            "mosaic",
            "undulatingburnout",
            "crosshatch",
            "crazyparametricfun",
            "kaleidoscope",
            "windowblinds",
            "hexagonalize",
            "hexagonalize",
            "dreamyzoom",
            "doomscreentransition_up",
            "doomscreentransition_down",
            "ripple",
            "pinwheel",
            "angular",
            "burn",
            "circle",
            "circleopen",
            "circleopen",
            "crosswarp",
            "cube",
            "directionalwipe",
            "doorway",
            "fade",
            "fadecolor",
            "fadegrayscale",
            "flyeye",
            "heart",
            "luma",
            "multiplyblend",
            "pixelize",
            "polarfunction",
            "randomsquares",
            "rotatescalefade",
            "squareswire",
            "squeeze",
            "swap",
            "wind"
    };


    @JSONField(serialize = false, deserialize = false)
    private static int subTypeSize = 63;

    @JSONField(name = "SubType")
    private String subType;

    @JSONField(name = "Duration")
    private float duration;

    public Transition() {}

    @Override
    public String getSubType() {
        return subType;
    }

    @Override
    public void setSubType(String subType) {
        this.subType = subType;
    }

    // 随机生成转场子类形
    public void setSubTypeRandom() {
        this.subType = subTypes[(int) (Math.random() * 100) % subTypeSize];
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }


}
