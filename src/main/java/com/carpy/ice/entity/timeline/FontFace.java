package com.carpy.ice.entity.timeline;

import com.alibaba.fastjson.annotation.JSONField;

// 字体样式
public class FontFace {

    @JSONField(name = "Bold")
    private boolean bold;

    @JSONField(name = "Italic")
    private boolean italic;

    @JSONField(name = "Underline")
    private boolean underline;

    public FontFace() {}

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public boolean isUnderline() {
        return underline;
    }

    public void setUnderline(boolean underline) {
        this.underline = underline;
    }
}
