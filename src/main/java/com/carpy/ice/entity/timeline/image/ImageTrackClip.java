package com.carpy.ice.entity.timeline.image;

import com.alibaba.fastjson.annotation.JSONField;
import com.carpy.ice.entity.timeline.effect.Effect;

import java.util.List;

// 图像轨素材ImageTrackClip的范围包括图片素材
public class ImageTrackClip {

    @JSONField(name = "MediaId")
    private String mediaId;

    @JSONField(name = "MediaURL", serialize=false)
    private String mediaURL;

    @JSONField(name = "X", serialize=false)
    private float x;

    @JSONField(name = "Y", serialize=false)
    private float y;

    @JSONField(name = "Width", serialize=false)
    private float width;

    @JSONField(name = "Height", serialize=false)
    private float height;

    @JSONField(name = "TimeLineIn", serialize=false)
    private float timeLineIn;

    @JSONField(name = "TimeLineOut", serialize=false)
    private float timeLineOut;

    @JSONField(name = "DyncFrames", serialize=false)
    private int dyncFrames;

    @JSONField(name = "Effents", serialize=false)
    private List<Effect> effects;

    public ImageTrackClip() {}

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaURL() {
        return mediaURL;
    }

    public void setMediaURL(String mediaURL) {
        this.mediaURL = mediaURL;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getTimeLineIn() {
        return timeLineIn;
    }

    public void setTimeLineIn(float timeLineIn) {
        this.timeLineIn = timeLineIn;
    }

    public float getTimeLineOut() {
        return timeLineOut;
    }

    public void setTimeLineOut(float timeLineOut) {
        this.timeLineOut = timeLineOut;
    }

    public int getDyncFrames() {
        return dyncFrames;
    }

    public void setDyncFrames(int dyncFrames) {
        this.dyncFrames = dyncFrames;
    }

    public List<Effect> getEffects() {
        return effects;
    }

    public void setEffects(List<Effect> effects) {
        this.effects = effects;
    }
}
