package com.carpy.ice.entity.timeline.effect;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

// 特效轨EffectTrack用于为视频整体添加特效，支持滤镜、VFX。
public class EffectTrack {
    @JSONField(name = "EffectTrackItems")
    private List<EffectTrackItem> effectTrackItems;

    public EffectTrack() {}

    public List<EffectTrackItem> getEffectTrackItems() {
        return effectTrackItems;
    }

    public void setEffectTrackItems(List<EffectTrackItem> effectTrackItems) {
        this.effectTrackItems = effectTrackItems;
    }
}
