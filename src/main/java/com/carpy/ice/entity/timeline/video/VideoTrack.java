package com.carpy.ice.entity.timeline.video;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

// 视频轨VideoTrack用于编排图像素材，包括视频素材和图片素材。
public class VideoTrack {

    @JSONField(name = "Type", serialize=false)
    private String type;

    @JSONField(name = "VideoTrackClips")
    private List<VideoTrackClip> videoTrackClips;

    public VideoTrack() {}

    public VideoTrack (List<String> videoMediaIdList) {
        this.videoTrackClips = new ArrayList<VideoTrackClip>();
        for(String videoMediaId: videoMediaIdList) {
            videoTrackClips.add(new VideoTrackClip(videoMediaId));
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<VideoTrackClip> getVideoTrackClips() {
        return videoTrackClips;
    }

    public void setVideoTrackClips(List<VideoTrackClip> videoTrackClips) {
        this.videoTrackClips = videoTrackClips;
    }
}
