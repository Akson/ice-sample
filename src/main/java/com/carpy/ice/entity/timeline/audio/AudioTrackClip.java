package com.carpy.ice.entity.timeline.audio;

import com.alibaba.fastjson.annotation.JSONField;
import com.carpy.ice.entity.timeline.effect.Effect;

import java.util.List;

// 音频轨素材AudioTrackClip的范围包括纯音频素材和有音频流的视频素材。
// In、Out、TimelineIn、TimelineOut标记了对素材截取的位置以及素材在时间线中的位置，如：想把一段视频的5~10s放在时间线15s~20s位置上，则：In=5, Out=10, TimelineIn=15, TimelineOut=20。
public class AudioTrackClip {

    @JSONField(name = "MediaId")
    private String mediaId;

    @JSONField(name = "MediaURL", serialize=false)
    private String mediaURL;

    @JSONField(name = "In", serialize=false)
    private float in;

    @JSONField(name = "Out", serialize=false)
    private float out;

    @JSONField(name = "TimeLineIn", serialize=false)
    private float timeLineIn;

    @JSONField(name = "TimeLineOut", serialize=false)
    private float timeLineOut;

    @JSONField(name = "Effents", serialize=false)
    private List<Effect> effects;

    public AudioTrackClip() {}

    public AudioTrackClip(String audioMediaId) {
        this.mediaId = audioMediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaURL() {
        return mediaURL;
    }

    public void setMediaURL(String mediaURL) {
        this.mediaURL = mediaURL;
    }

    public float getIn() {
        return in;
    }

    public void setIn(float in) {
        this.in = in;
    }

    public float getOut() {
        return out;
    }

    public void setOut(float out) {
        this.out = out;
    }

    public float getTimeLineIn() {
        return timeLineIn;
    }

    public void setTimeLineIn(float timeLineIn) {
        this.timeLineIn = timeLineIn;
    }

    public float getTimeLineOut() {
        return timeLineOut;
    }

    public void setTimeLineOut(float timeLineOut) {
        this.timeLineOut = timeLineOut;
    }

    public List<Effect> getEffects() {
        return effects;
    }

    public void setEffects(List<Effect> effects) {
        this.effects = effects;
    }
}
