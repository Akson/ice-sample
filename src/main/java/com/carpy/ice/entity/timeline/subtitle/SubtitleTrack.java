package com.carpy.ice.entity.timeline.subtitle;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

// 字幕轨SubtitleTrack用于编排字幕，包括横幅文字和外挂字幕。
public class SubtitleTrack {

    @JSONField(name = "SubtitleTrackClips")
    private List<SubtitleTrackClip> subtitleTrackClips;

    public SubtitleTrack() {}

    public List<SubtitleTrackClip> getSubtitleTrackClips() {
        return subtitleTrackClips;
    }

    public void setSubtitleTrackClips(List<SubtitleTrackClip> subtitleTrackClips) {
        this.subtitleTrackClips = subtitleTrackClips;
    }
}
