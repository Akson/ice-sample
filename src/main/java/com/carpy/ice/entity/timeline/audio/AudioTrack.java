package com.carpy.ice.entity.timeline.audio;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

// 音频轨AudioTrack用于编排音频素材，包括纯音频素材和带音频的视频素材。
public class AudioTrack {
    @JSONField(name = "AudioTrackClips")
    private List<AudioTrackClip> audioTrackClips;

    public AudioTrack() {}

    public AudioTrack(List<String> audioMediaIdList) {
        this.audioTrackClips = new ArrayList<AudioTrackClip>();

        for(String audioMediaId: audioMediaIdList) {
            this.audioTrackClips.add(new AudioTrackClip(audioMediaId));
        }
    }

    public List<AudioTrackClip> getAudioTrackClips() {
        return audioTrackClips;
    }

    public void setAudioTrackClips(List<AudioTrackClip> audioTrackClips) {
        this.audioTrackClips = audioTrackClips;
    }
}
