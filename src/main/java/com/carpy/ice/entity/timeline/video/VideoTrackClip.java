package com.carpy.ice.entity.timeline.video;

import com.alibaba.fastjson.annotation.JSONField;
import com.carpy.ice.entity.timeline.effect.Effect;

import java.util.List;

// 视频轨素材VideoTrackClip的范围包括视频素材和图片素材。
// In、Out、TimelineIn、TimelineOut标记了对素材截取的位置以及素材在时间线中的位置，如：想把一段视频的5~10s放在时间线15s~20s位置上，则：In=5, Out=10, TimelineIn=15, TimelineOut=20。
public class VideoTrackClip {

    @JSONField(name = "MediaId")
    private String mediaId;

    @JSONField(name = "MediaURL", serialize=false)
    private String mediaURL;

    @JSONField(name = "Type", serialize=false)
    private float type;

    @JSONField(name = "In", serialize=false)
    private float in;

    @JSONField(name = "Out", serialize=false)
    private float out;

    @JSONField(name = "Duration", serialize=false)
    private float duration;

    @JSONField(name = "DyncFrames", serialize=false)
    private int dyncFrames;

    @JSONField(name = "TimeLineIn", serialize=false)
    private float timeLineIn;

    @JSONField(name = "TimeLineOut", serialize=false)
    private float timeLineOut;

    @JSONField(name = "Effects")
    private List<Effect> effects;

    public VideoTrackClip() {}

    public VideoTrackClip(String videoMediaId) {
        this.mediaId = videoMediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaURL() {
        return mediaURL;
    }

    public void setMediaURL(String mediaURL) {
        this.mediaURL = mediaURL;
    }

    public float getType() {
        return type;
    }

    public void setType(float type) {
        this.type = type;
    }

    public float getIn() {
        return in;
    }

    public void setIn(float in) {
        this.in = in;
    }

    public float getOut() {
        return out;
    }

    public void setOut(float out) {
        this.out = out;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public int getDyncFrames() {
        return dyncFrames;
    }

    public void setDyncFrames(int dyncFrames) {
        this.dyncFrames = dyncFrames;
    }

    public float getTimeLineIn() {
        return timeLineIn;
    }

    public void setTimeLineIn(float timeLineIn) {
        this.timeLineIn = timeLineIn;
    }

    public float getTimeLineOut() {
        return timeLineOut;
    }

    public void setTimeLineOut(float timeLineOut) {
        this.timeLineOut = timeLineOut;
    }

    public List<Effect> getEffects() {
        return effects;
    }

    public void setEffects(List<Effect> effects) {
        this.effects = effects;
    }
}
