package com.carpy.ice.entity.timeline.effect;

import com.alibaba.fastjson.annotation.JSONField;

// 素材效果
public class Effect {
    @JSONField(name = "Type")
    private String type;

    @JSONField(name = "SubType")
    private String subType;

    @JSONField(name = "TimeLineIn", serialize=false)
    private float timeLineIn;

    @JSONField(name = "TimeLineOut", serialize=false)
    private float timeLineOut;

    public Effect() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public float getTimeLineIn() {
        return timeLineIn;
    }

    public void setTimeLineIn(float timeLineIn) {
        this.timeLineIn = timeLineIn;
    }

    public float getTimeLineOut() {
        return timeLineOut;
    }

    public void setTimeLineOut(float timeLineOut) {
        this.timeLineOut = timeLineOut;
    }
}
