package com.carpy.ice.entity.timeline.subtitle;

import com.alibaba.fastjson.annotation.JSONField;
import com.carpy.ice.entity.timeline.FontFace;

// 字幕轨素材
public class SubtitleTrackClip {
    @JSONField(name = "Type")
    private String type;

    @JSONField(name = "SubType")
    private String subType;

    @JSONField(name = "FileURL")
    private String fileURL;

    @JSONField(name = "X", serialize=false)
    private float x;

    @JSONField(name = "Y", serialize=false)
    private float y;

    @JSONField(name = "TimeLineIn", serialize=false)
    private float timeLineIn;

    @JSONField(name = "TimeLineOut", serialize=false)
    private float timeLineOut;

    @JSONField(name = "Content", serialize=false)
    private String content;

    @JSONField(name = "Font", serialize=false)
    private String font;

    @JSONField(name = "FontSize", serialize=false)
    private int fontSize;

    @JSONField(name = "FontColor", serialize=false)
    private String FontColor;

    @JSONField(name = "FontColorOpacity", serialize=false)
    private String fontColorOpacity;

    @JSONField(name = "FontFace", serialize=false)
    private FontFace fontFace;

    public SubtitleTrackClip() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getTimeLineIn() {
        return timeLineIn;
    }

    public void setTimeLineIn(float timeLineIn) {
        this.timeLineIn = timeLineIn;
    }

    public float getTimeLineOut() {
        return timeLineOut;
    }

    public void setTimeLineOut(float timeLineOut) {
        this.timeLineOut = timeLineOut;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontColor() {
        return FontColor;
    }

    public void setFontColor(String fontColor) {
        FontColor = fontColor;
    }

    public String getFontColorOpacity() {
        return fontColorOpacity;
    }

    public void setFontColorOpacity(String fontColorOpacity) {
        this.fontColorOpacity = fontColorOpacity;
    }

    public FontFace getFontFace() {
        return fontFace;
    }

    public void setFontFace(FontFace fontFace) {
        this.fontFace = fontFace;
    }
}
