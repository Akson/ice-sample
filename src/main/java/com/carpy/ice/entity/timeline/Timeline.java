package com.carpy.ice.entity.timeline;

import com.alibaba.fastjson.annotation.JSONField;
import com.carpy.ice.entity.timeline.audio.AudioTrack;
import com.carpy.ice.entity.timeline.effect.EffectTrack;
import com.carpy.ice.entity.timeline.image.ImageTrack;
import com.carpy.ice.entity.timeline.subtitle.SubtitleTrack;
import com.carpy.ice.entity.timeline.video.VideoTrack;

import java.util.ArrayList;
import java.util.List;

public class Timeline {

    @JSONField(name = "VideoTracks")
    private List<VideoTrack> videoTracks;

    @JSONField(name = "AudioTracks")
    private List<AudioTrack> audioTracks;

    @JSONField(name = "ImageTracks")
    private List<ImageTrack> imageTracks;

    @JSONField(name = "SubtitleTracks")
    private List<SubtitleTrack> SubtitleTracks;

    @JSONField(name = "EffectTracks")
    private List<EffectTrack> effectTracks;

    public Timeline() {}

    public Timeline(List<String> videoMediaIdList, List<String> audioMediaIdList) {
        List<VideoTrack> videoTracks= new ArrayList<VideoTrack>();
        videoTracks.add(new VideoTrack(videoMediaIdList));

        List<AudioTrack> audioTracks= new ArrayList<AudioTrack>();
        audioTracks.add(new AudioTrack(audioMediaIdList));

        this.videoTracks = videoTracks;
        this.audioTracks = audioTracks;
    }

    public List<VideoTrack> getVideoTracks() {
        return videoTracks;
    }

    public void setVideoTracks(List<VideoTrack> videoTracks) {
        this.videoTracks = videoTracks;
    }

    public List<AudioTrack> getAudioTracks() {
        return audioTracks;
    }

    public void setAudioTracks(List<AudioTrack> audioTracks) {
        this.audioTracks = audioTracks;
    }

    public List<ImageTrack> getImageTracks() {
        return imageTracks;
    }

    public void setImageTracks(List<ImageTrack> imageTracks) {
        this.imageTracks = imageTracks;
    }

    public List<SubtitleTrack> getSubtitleTracks() {
        return SubtitleTracks;
    }

    public void setSubtitleTracks(List<SubtitleTrack> subtitleTracks) {
        SubtitleTracks = subtitleTracks;
    }

    public List<EffectTrack> getEffectTracks() {
        return effectTracks;
    }

    public void setEffectTracks(List<EffectTrack> effectTracks) {
        this.effectTracks = effectTracks;
    }
}
