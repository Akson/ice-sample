package com.carpy.ice.entity.timeline.effect;

import com.alibaba.fastjson.annotation.JSONField;

// 特效轨片段EffectTrackItem，类型包括VFX、滤镜
public class EffectTrackItem {
    @JSONField(name = "Type")
    private String type;

    @JSONField(name = "SubType")
    private String subType;

    @JSONField(name = "TimeLineIn", serialize=false)
    private float timeLineIn;

    @JSONField(name = "TimeLineOut", serialize=false)
    private float timeLineOut;

    @JSONField(name = "Duration")
    private float duration;

    @JSONField(name = "X", serialize=false)
    private float x;

    @JSONField(name = "Y", serialize=false)
    private float y;

    @JSONField(name = "Width", serialize=false)
    private float width;

    @JSONField(name = "Height", serialize=false)
    private float height;

    public EffectTrackItem() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public float getTimeLineIn() {
        return timeLineIn;
    }

    public void setTimeLineIn(float timeLineIn) {
        this.timeLineIn = timeLineIn;
    }

    public float getTimeLineOut() {
        return timeLineOut;
    }

    public void setTimeLineOut(float timeLineOut) {
        this.timeLineOut = timeLineOut;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
