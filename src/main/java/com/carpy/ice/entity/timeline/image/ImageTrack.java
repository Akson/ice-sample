package com.carpy.ice.entity.timeline.image;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

// 图片轨ImageTrack用于编排图片素材。
public class ImageTrack {

    @JSONField(name = "ImageTrackClips")
    private List<ImageTrackClip> imageTrackClips;

    public ImageTrack() {}

    public List<ImageTrackClip> getImageTrackClips() {
        return imageTrackClips;
    }

    public void setImageTrackClips(List<ImageTrackClip> imageTrackClips) {
        this.imageTrackClips = imageTrackClips;
    }
}
