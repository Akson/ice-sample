/*
1. 读取视频资源，将其上传至自己的oss bucket中，并注册媒资，获得媒资id
 */
package com.carpy.ice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONReader;
import com.carpy.ice.entity.BasicConfig;
import com.carpy.ice.entity.timeline.Timeline;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main( String[] args ) throws Exception{
        // 第1步, 解析配置信息
        JSONReader reader = new JSONReader(new FileReader("user_config.json"));
        BasicConfig basicConfig = reader.readObject(BasicConfig.class);

        HellOss hellOss = new HellOss(basicConfig);
        HelloIce helloIce = new HelloIce(basicConfig);

        // 第2步, 准备视频资源
        // 读取当前项目目录(assets/video)下的视频资源, 将它们上传到OSS中，注册成媒资，记录MediaId
        List<String> videoMediaFileList = hellOss.prepareVideoResource();
        List<String> videoMediaIdList = helloIce.registerMedia(videoMediaFileList, "video");

        // 第3步, 准备语音资源
        // 读取当前项目目录(assets/text)下的语料资源, 使用ice的智能语音功能来转为语音, 并记录MediaId
        List<String> audioMediaIdList = helloIce.textToAudio();

        // 第4步, 使用准备好的视频媒资和音频媒资执行剪辑合成作业
        String videoMediaId = helloIce.mediaProducingJobTest(videoMediaIdList, audioMediaIdList);

        // 第5步，使用ASR来从生成完的视频中提取字幕
        String subtitleFileName = helloIce.asrJobTest(videoMediaId);
        List<String> subtitleFileList = hellOss.prepareSubtitleResource();

        // 第6步，融合视频与字幕
        videoMediaIdList = new ArrayList<String>();
        videoMediaIdList.add(videoMediaId);
        List<String> productInfo = helloIce.mergeVideoAndSubtitle(videoMediaIdList, subtitleFileList);

        System.out.println("从Oss中下载最终视频：" + productInfo.get(1));
        hellOss.downloadFileFromBucket(productInfo.get(1));
    }
}
