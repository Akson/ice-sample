package com.carpy.ice;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.tea.*;
import com.aliyun.ice20201109.*;
import com.aliyun.ice20201109.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;
import com.aliyuncs.ram.model.v20150501.GetUserResponse;
import com.carpy.ice.entity.BasicConfig;
import com.carpy.ice.entity.OutputMediaConfig;
import com.carpy.ice.entity.timeline.Timeline;
import com.carpy.ice.entity.timeline.audio.AudioTrack;
import com.carpy.ice.entity.timeline.audio.AudioTrackClip;
import com.carpy.ice.entity.timeline.effect.Effect;
import com.carpy.ice.entity.timeline.effect.Transition;
import com.carpy.ice.entity.timeline.subtitle.SubtitleTrack;
import com.carpy.ice.entity.timeline.subtitle.SubtitleTrackClip;
import com.carpy.ice.entity.timeline.video.VideoTrack;
import com.carpy.ice.entity.timeline.video.VideoTrackClip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class HelloIce {
    private BasicConfig basicConfig;

    public HelloIce(BasicConfig basicConfig) {
        this.basicConfig = basicConfig;
    }

    // 注册媒资
    public List<String> registerMedia(List<String> mediaList, String mediaType) {
        List<String> mediaIdList = new ArrayList<>();

        try {
            com.aliyun.ice20201109.Client iceClient = createClient();
            for (String mediaName : mediaList) {
                String url = "oss://" + this.basicConfig.getBucketName() + "/" + mediaName;
                RegisterMediaInfoRequest req = new RegisterMediaInfoRequest();
                req.setInputURL(url);
                req.setMediaType(mediaType);
                // System.out.println(url);
                RegisterMediaInfoResponse res = iceClient.registerMediaInfo(req);
                mediaIdList.add(res.getBody().getMediaId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mediaIdList;
    }

    // 文字转声音
    public List<String> textToAudio() {
        List<String> audioMediaIdList = new ArrayList<String>();
        Queue<String> jobIdQueue = new LinkedList<String>();

        try {
            com.aliyun.ice20201109.Client iceClient = createClient();

            for (File file : new File(this.basicConfig.getTextPath()).listFiles()) {
                if (file.isHidden()) continue;

                System.out.println();
                SubmitAudioProduceJobRequest submitAudioProduceJobRequest = new SubmitAudioProduceJobRequest()
                        .setEditingConfig("{\"voice\":\"Siqi\",\"format\":\"MP3\", \"speech_rate\": -50}")
                        .setOutputConfig("{\"bucket\":\"carp-mt\",\"object\":\"" + this.basicConfig.getUniquePre() + file.getName().split("\\.")[0] + "\"}")
                        .setInputConfig(getContent(file));

                SubmitAudioProduceJobResponse res = iceClient.submitAudioProduceJob(submitAudioProduceJobRequest);
                jobIdQueue.offer(res.getBody().getJobId());
            }

            while(!jobIdQueue.isEmpty()) {
                GetSmartHandleJobRequest req = new GetSmartHandleJobRequest().setJobId(jobIdQueue.poll());
                while(true) {
                    Thread.sleep(5000);

                    GetSmartHandleJobResponse res = iceClient.getSmartHandleJob(req);

                    if(res.getBody().getState().equals("Finished")) {
                        GetMediaInfoRequest getMediaInfoRequest = new GetMediaInfoRequest().setInputURL("oss://" + this.basicConfig.getBucketName() + "/" + res.getBody().getSmartJobInfo().getOutputConfig().getObject() + ".MP3");
                        GetMediaInfoResponse getMediaInfoResponse = iceClient.getMediaInfo(getMediaInfoRequest);
                        audioMediaIdList.add(getMediaInfoResponse.getBody().getMediaInfo().getMediaId());
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return audioMediaIdList;
    }

    // 剪辑合成作业
    public String mediaProducingJobTest(List<String> videoMediaIdList, List<String> audioMediaIdList) {

        // 这里我们的需求是: 顺序播放video，添加转场效果，并添加背景音乐

        // 第1步：依据给定的videoMediaIdList 和 audioMediaIdList 生成timeline配置
        Timeline timeline = genTimeline(videoMediaIdList, audioMediaIdList);
        System.out.println(JSON.toJSONString(timeline));

        // 第2步：配置作业并提交
        String videoMediaId = new String();
        try {
            com.aliyun.ice20201109.Client iceClient = createClient();
            String videoName = this.basicConfig.getUniquePre() + "video_edit.mp4";
            String mediaURL = "https://" + this.basicConfig.getBucketName() + "." + this.basicConfig.getOssEndpoint() + "/" + videoName;

            OutputMediaConfig outputMediaConfig = new OutputMediaConfig();
            outputMediaConfig.setMediaURL(mediaURL);
            outputMediaConfig.setBitrate(1000);
            outputMediaConfig.setHeight(480);
            outputMediaConfig.setWidth(600);

            SubmitMediaProducingJobRequest submitMediaProducingJobRequest = new SubmitMediaProducingJobRequest()
                    .setTimeline(JSON.toJSONString(timeline))
                    .setOutputMediaTarget("oss-object")
                    .setOutputMediaConfig(JSON.toJSONString(outputMediaConfig));

            SubmitMediaProducingJobResponse submitMediaProducingJobResponse = iceClient.submitMediaProducingJob(submitMediaProducingJobRequest);
            videoMediaId = submitMediaProducingJobResponse.getBody().getMediaId();

            GetMediaProducingJobRequest getMediaProducingJobRequest = new GetMediaProducingJobRequest().setJobId(submitMediaProducingJobResponse.getBody().getJobId());

            System.out.print("剪辑合成中");
            while(true) {
                System.out.print(".");
                Thread.sleep(5000);
                GetMediaProducingJobResponse getMediaProducingJobResponse = iceClient.getMediaProducingJob(getMediaProducingJobRequest);
                String status = getMediaProducingJobResponse.getBody().getMediaProducingJob().getStatus();

                if(status.equals("Success")) {
                    System.out.println("\n剪辑合成成功");
                    break;
                }

                if(status.equals("Failed")) {
                    System.out.println("\n剪辑合成失败");
                    System.exit(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return videoMediaId;
    }

    // asr智能语音任务
    public String asrJobTest(String videoMediaId) {
        try {
            com.aliyun.ice20201109.Client iceClient = createClient();

            String objFileName = "subtitle";
            SubmitASRJobRequest submitASRJobRequest = new SubmitASRJobRequest().setInputFile(videoMediaId).setTitle(objFileName);
            SubmitASRJobResponse submitASRJobResponse = iceClient.submitASRJob(submitASRJobRequest);

            System.out.print("智能提取字幕中");
            GetSmartHandleJobRequest getSmartHandleJobRequest = new GetSmartHandleJobRequest().setJobId(submitASRJobResponse.getBody().getJobId());
            while(true) {
                System.out.print(".");
                Thread.sleep(5000);

                GetSmartHandleJobResponse getSmartHandleJobResponse = iceClient.getSmartHandleJob(getSmartHandleJobRequest);

                if(getSmartHandleJobResponse.getBody().getState().equals("Finished")) {
                    System.out.println("\n提取字幕成功");

                    System.out.println(getSmartHandleJobResponse.getBody().getOutput());

                    String subtitleFileName = "subtitle.srt";
                    SolveSubtitle solveSubtitle = new SolveSubtitle();
                    String filePath = solveSubtitle.solve(getSmartHandleJobResponse.getBody().getOutput(), subtitleFileName);

                    return filePath;
                }

                if(getSmartHandleJobResponse.getBody().getState().equals("Failed")) {
                    System.out.println("\n提取字幕失败");
                    System.exit(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // 合并视频和字幕
    public List<String> mergeVideoAndSubtitle(List<String> videoMediaIdList, List<String> subtitleFileNameList) {

        // 第1步：配置时间线
        Timeline timeline = new Timeline();
        List<VideoTrack> videoTracks = new ArrayList<VideoTrack>();
        List<SubtitleTrack> subtitleTracks = new ArrayList<SubtitleTrack>();

        VideoTrack videoTrack = new VideoTrack();
        SubtitleTrack subtitleTrack = new SubtitleTrack();

        List<VideoTrackClip> videoTrackClips = new ArrayList<VideoTrackClip>();
        List<SubtitleTrackClip> subtitleTrackClips = new ArrayList<SubtitleTrackClip>();

        for(String videoMediaId: videoMediaIdList) {
            VideoTrackClip videoTrackClip = new VideoTrackClip();
            videoTrackClip.setMediaId(videoMediaId);

            videoTrackClips.add(videoTrackClip);
        }

        for(String subtitleFileName: subtitleFileNameList) {
            String fileURL = "https://" + this.basicConfig.getBucketName() + '.' + this.basicConfig.getOssEndpoint() + "/" + subtitleFileName;

            SubtitleTrackClip subtitleTrackClip = new SubtitleTrackClip();
            subtitleTrackClip.setFileURL(fileURL);
            subtitleTrackClip.setType("Subtitle");
            subtitleTrackClip.setSubType("srt");

            subtitleTrackClips.add(subtitleTrackClip);
        }

        videoTrack.setVideoTrackClips(videoTrackClips);
        subtitleTrack.setSubtitleTrackClips(subtitleTrackClips);

        videoTracks.add(videoTrack);
        subtitleTracks.add(subtitleTrack);

        timeline.setVideoTracks(videoTracks);
        timeline.setSubtitleTracks(subtitleTracks);

        System.out.println("融合视频和字幕的时间线：" + JSON.toJSONString(timeline));
        // 第2步：配置作业并提交
        String videoName = this.basicConfig.getUniquePre() + "video_product.mp4";
        String mediaURL = "https://" + this.basicConfig.getBucketName() + "." + this.basicConfig.getOssEndpoint() + "/" + videoName;
        String videoMediaId = new String();
        try {
            com.aliyun.ice20201109.Client iceClient = createClient();

            OutputMediaConfig outputMediaConfig = new OutputMediaConfig();
            outputMediaConfig.setMediaURL(mediaURL);
            outputMediaConfig.setBitrate(1000);
            outputMediaConfig.setHeight(480);
            outputMediaConfig.setWidth(600);

            SubmitMediaProducingJobRequest submitMediaProducingJobRequest = new SubmitMediaProducingJobRequest()
                    .setTimeline(JSON.toJSONString(timeline))
                    .setOutputMediaTarget("oss-object")
                    .setOutputMediaConfig(JSON.toJSONString(outputMediaConfig));

            SubmitMediaProducingJobResponse submitMediaProducingJobResponse = iceClient.submitMediaProducingJob(submitMediaProducingJobRequest);
            videoMediaId = submitMediaProducingJobResponse.getBody().getMediaId();

            GetMediaProducingJobRequest getMediaProducingJobRequest = new GetMediaProducingJobRequest().setJobId(submitMediaProducingJobResponse.getBody().getJobId());

            System.out.print("剪辑合成中");
            while(true) {
                System.out.print(".");
                Thread.sleep(5000);
                GetMediaProducingJobResponse getMediaProducingJobResponse = iceClient.getMediaProducingJob(getMediaProducingJobRequest);
                String status = getMediaProducingJobResponse.getBody().getMediaProducingJob().getStatus();

                if(status.equals("Success")) {
                    System.out.println("\n剪辑合成成功");
                    break;
                }

                if(status.equals("Failed")) {
                    System.out.println("\n剪辑合成失败");
                    System.exit(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        List<String> res = new ArrayList<String>();
        res.add(videoMediaId);
        res.add(videoName);
        return res;
    }

    // 依据视频列表和音频列表生成时间线，均为单轨
    private Timeline genTimeline(List<String> videoMediaIdList, List<String> audioMediaIdList) {
        Timeline timeline = new Timeline();

        List<VideoTrack> videoTracks = new ArrayList<VideoTrack>();
        List<AudioTrack> audioTracks = new ArrayList<AudioTrack>();

        // 视频轨列表和音频轨列表
        VideoTrack videoTrack = new VideoTrack();
        AudioTrack audioTrack = new AudioTrack();

        // 这个需求比较简单，只需要一条视频轨和一条音频轨
        List<VideoTrackClip> videoTrackClips = new ArrayList<VideoTrackClip>();
        List<AudioTrackClip> audioTrackClips = new ArrayList<AudioTrackClip>();

        // 配置视频轨
        for(String videoMediaId: videoMediaIdList) {
            VideoTrackClip videoTrackClip = new VideoTrackClip();
            videoTrackClip.setMediaId(videoMediaId);

            // 配置转场
            List<Effect> effects = new ArrayList<>();
            Transition transition = new Transition();
            transition.setType("Transition");
            transition.setSubTypeRandom();
            transition.setDuration(1);

            effects.add(transition);
            videoTrackClip.setEffects(effects);

            videoTrackClips.add(videoTrackClip);
        }

        videoTrack.setVideoTrackClips(videoTrackClips);
        videoTracks.add(videoTrack);

        // 配置音频轨
        for(String audioMediaId: audioMediaIdList) {
            AudioTrackClip audioTrackClip = new AudioTrackClip();
            audioTrackClip.setMediaId(audioMediaId);

            audioTrackClips.add(audioTrackClip);
        }

        audioTrack.setAudioTrackClips(audioTrackClips);
        audioTracks.add(audioTrack);

        timeline.setVideoTracks(videoTracks);
        timeline.setAudioTracks(audioTracks);

        return timeline;
    }

    // 获取某一文件内的全部内容
    private String getContent(File file) {
        String encoding = "UTF-8";

        byte[] fileContent = new byte[(int) file.length()];

        try {
            FileInputStream is = new FileInputStream(file);
            is.read(fileContent);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            return new String(fileContent, encoding);
        } catch (Exception e) {
            return null;
        }
    }

    private String getConfig(String configFile) throws Exception {
        BufferedReader bf = new BufferedReader(new FileReader(configFile));

        String content = new String();
        String line;
        while ((line = bf.readLine()) != null) {
            content += line;
        }

        return content;
    }

    private void audioProduceJobTest() throws Exception {
        com.aliyun.ice20201109.Client client = createClient();

        File textDir = new File("assets/text");
        String content = new String();

        for(String fileName : textDir.list()) {
            // System.out.println(fileName);
            File textFile = new File(textDir + "/" + fileName);
            FileReader fileReader = new FileReader(textFile);
            BufferedReader br = new BufferedReader(fileReader);

            String line;
            while((line = br.readLine()) != null) {
                content += '\n' + line;
            }
        }

        SubmitAudioProduceJobRequest submitAudioProduceJobRequest = new SubmitAudioProduceJobRequest()
                .setEditingConfig("{\"voice\":\"Siqi\",\"format\":\"MP3\", \"speech_rate\": -50}")
                .setOutputConfig("{\"bucket\":\"carp-mt\",\"object\":\"a_1\"}")
                .setInputConfig(content)
                .setOverwrite(true)
                .setTitle("carp");

        SubmitAudioProduceJobResponse res = client.submitAudioProduceJob(submitAudioProduceJobRequest);
        System.out.println("JobId: " + res.getBody().getJobId());
        System.out.println("State: " + res.getBody().getState());
    }

    private void mediaBasicInfoTest(com.aliyun.ice20201109.Client iceClient) {
        try {
            ListMediaBasicInfosRequest listMediaBasicInfosRequest = new ListMediaBasicInfosRequest();
            ListMediaBasicInfosResponse listMediaBasicInfosResponse = iceClient.listMediaBasicInfos(listMediaBasicInfosRequest);
            System.out.println("MaxResults : " + listMediaBasicInfosResponse.getBody().getMaxResults());
            if (listMediaBasicInfosResponse.getBody().getMediaInfos() != null &&
                    listMediaBasicInfosResponse.getBody().getMediaInfos().size() > 0) {
                System.out.println("size : " + listMediaBasicInfosResponse.getBody().getMediaInfos().size());
                System.out.println("mediaid : " + listMediaBasicInfosResponse.getBody().getMediaInfos().get(0).getMediaId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private com.aliyun.ice20201109.Client createClient() throws Exception {
        Config config = new Config()
                .setAccessKeyId(this.basicConfig.getAccessKeyId())
                .setAccessKeySecret(this.basicConfig.getAccessKeySecret())
                .setEndpoint(this.basicConfig.getIceEndpoint())
                .setRegionId(this.basicConfig.getRegionID());
        return new com.aliyun.ice20201109.Client(config);
    }
}
