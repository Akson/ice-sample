package com.carpy.ice;

import com.alibaba.fastjson.JSON;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class SolveSubtitle {
    public static class SubtitleLine {
        public String content;
        public double from;
        public double to;
    }

    public SolveSubtitle() {}

    public String solve(String content, String fileName) {
        List<SubtitleLine> subtitleLines = JSON.parseArray(content, SubtitleLine.class);
        String filePath = new String();
        try {
            filePath = "assets/subtitle/" + fileName;
            File f = new File(filePath);
            if(f.exists()) f.delete();

            f.createNewFile();
            FileWriter fileWriter = new FileWriter(f, true);
            int i = 1;
            for(SubtitleLine subtitleLine : subtitleLines) {
                String line1 = i + "\r";
                fileWriter.append(line1);
                String line2 = parseTime(subtitleLine.from) + " --> " + parseTime(subtitleLine.to) + "\r";
                fileWriter.append(line2);
                fileWriter.append(subtitleLine.content + "\r");
                fileWriter.append("\r");

                i++;
            }

            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return filePath;
    }

    private String parseTime(double time) {
        int second = (int)Math.floor(time);
        time -= (double)second;

        int hour = second / 3600;
        second %= 3600;

        int minute = second / 60;
        second %= 60;

        return String.format("%02d:%02d:%02d,%03d", hour, minute, second, (int)Math.floor(time * 1000));
    }
}


