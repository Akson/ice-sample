package com.carpy.ice;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import com.carpy.ice.entity.BasicConfig;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class HellOss {
    private BasicConfig basicConfig;

    public HellOss(BasicConfig basicConfig) {
        this.basicConfig = basicConfig;
    }

    // 准备视频资源
    public List<String> prepareVideoResource() {
        // 初始化OSS客户端
        OSS ossClient = new OSSClientBuilder().build(this.basicConfig.getOssEndpoint(), this.basicConfig.getAccessKeyId(), this.basicConfig.getAccessKeySecret());

        System.out.println("上传视频文件...");
        // 读取上传文件列表
        List<String> videoList = uploadFilesToOss(this.basicConfig.getVideoPath(), ossClient);

        System.out.println("上传视频文件完成。");
        Collections.sort(videoList);
        return videoList;
    }

    // 准备字幕资源
    public List<String> prepareSubtitleResource() {
        // 初始化OSS客户端
        OSS ossClient = new OSSClientBuilder().build(this.basicConfig.getOssEndpoint(), this.basicConfig.getAccessKeyId(), this.basicConfig.getAccessKeySecret());

        System.out.println("上传字幕文件...");
        // 读取上传文件列表
        List<String> subtitleList = uploadFilesToOss(this.basicConfig.getSubtitlePath(), ossClient);

        System.out.println("上传字幕文件完成。");
        Collections.sort(subtitleList);
        return subtitleList;
    }

    // 辅助函数：批量上传某一目录下的所有文件
    private List<String> uploadFilesToOss(String fileDir, OSS ossClient) {
        List<String> fileList = new ArrayList<>();

        for (File file : new File(fileDir).listFiles()) {
            if(file.isHidden()) continue;

            String key = this.basicConfig.getUniquePre() + file.getName();
            System.out.println(key);

            ossClient.putObject(new PutObjectRequest(this.basicConfig.getBucketName(), key, file));

            System.out.println("上传文件 \"" + key + "\"- 成功");
            fileList.add(key);
        }

        return fileList;
    }

    // 下载文件
    public void downloadFileFromBucket(String objectName) {
        try {
            OSS ossClient = new OSSClientBuilder().build(this.basicConfig.getOssEndpoint(), this.basicConfig.getAccessKeyId(), this.basicConfig.getAccessKeySecret());

            // 下载Object到本地文件，并保存到指定的本地路径中。如果指定的本地文件存在会覆盖，不存在则新建。
            // 如果未指定本地路径，则下载后的文件默认保存到示例程序所属项目对应本地路径中。
            ossClient.getObject(new GetObjectRequest(this.basicConfig.getBucketName(), objectName), new File(objectName));

            ossClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearBucket() {
        OSS ossClient = new OSSClientBuilder().build(this.basicConfig.getOssEndpoint(), this.basicConfig.getAccessKeyId(), this.basicConfig.getAccessKeySecret());

        ObjectListing objectList = ossClient.listObjects(this.basicConfig.getBucketName());
        List<OSSObjectSummary> sums = objectList.getObjectSummaries();

        for(OSSObjectSummary s : sums) {
            ossClient.deleteObject(this.basicConfig.getBucketName(), s.getKey());
        }

        ossClient.shutdown();
    }

    // 获取bucket列表测试
    private void getBucketsTest() {
        OSS ossClient = new OSSClientBuilder().build(this.basicConfig.getOssEndpoint(), this.basicConfig.getAccessKeyId(), this.basicConfig.getAccessKeySecret());

        List<Bucket> buckets = ossClient.listBuckets();
        for(Bucket bucket : buckets) {
            System.out.println(" - " + bucket.getName());
        }

        ossClient.shutdown();
    }

    private File getFile() {
        String filePath = "~/Movie/VideoSource/v_1.mp4";

        File file = new File(filePath);
        System.out.println(file.getName());
        return file;
    }

    public void file_test() {
        String dirPath = "./";
        File assetsDir = new File(dirPath);
        System.out.println(assetsDir.getAbsolutePath());

        for( String iter : assetsDir.list()) {
            System.out.println(iter);
        }
    }
}
